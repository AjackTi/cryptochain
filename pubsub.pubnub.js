const PubNub = require('pubnub')

const credentials = {
    publishKey: 'pub-c-bc9e303b-4896-410a-a6ea-412791e9d3a0',
    subscribeKey: 'sub-c-e89ab3ea-0623-11ea-bcd6-ead0b8c5d242',
    secretKey: 'sec-c-ZTE0YjFjYzMtNmU0Mi00YmIyLTlmNTQtMTYwM2M4ZDBkMzMw'
};

const CHANNELS = {
    TEST: 'TEST'
}

class Pubsub {
    constructor() {
        this.pubnub = new PubNub(credentials);

        this.pubnub.subscribe({channels: Object.values(CHANNELS)});

        this.pubnub.addListener(this.listener());
    }

    listener() {
        return {
            message: messageObject => {
                const {channel, message} = messageObject;

                console.log(`Message received. Channel: ${channel}. Message: ${message}`)
            }
        }
    }

    publish({channel, message}) {
        this.pubnub.publish({channel, message})
    }
}

// const testPubSub = new Pubsub();
// testPubSub.publish({channel: CHANNELS.TEST, message: 'hello pubnub'})

module.exports = Pubsub;