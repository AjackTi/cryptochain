const Transaction = require('./transaction')
const Wallet = require('./index')
const {verifySignature} = require("../util");

describe('Transaction', () => {
    let transaction, senderWallet, recipient, amount;

    beforeEach(() => {
        senderWallet = new Wallet();
        recipient = 'recipient-public-key';
        amount = 50;

        transaction = new Transaction({senderWallet, recipient, amount});
    });

    it('has an `id`', function () {
        expect(transaction).toHaveProperty('id');
    });

    describe('outputMap', () => {
        it('has an `outputMap`', function () {
            expect(transaction).toHaveProperty('outputMap')
        });

        it('outputs the amount to the recipient', function () {
            expect(transaction.outputMap[recipient]).toEqual(amount);
        });

        it('outputs the remaining balance for the `senderWallet`', function () {
            expect(transaction.outputMap[senderWallet.publicKey]).toEqual(senderWallet.balance - amount);
        });
    });

    describe('input', () => {
        it('has an `input`', function () {
            expect(transaction).toHaveProperty('input');
        });

        it('has a `timestamp` in the input', function () {
            expect(transaction.input).toHaveProperty('timestamp')
        });

        it('sets the `amount` to the `senderWallet` balance', function () {
            expect(transaction.input.amount).toEqual(senderWallet.balance);
        });

        it('sets the `address` to the `senderWallet` publicKey', function () {
            expect(transaction.input.address).toEqual(senderWallet.publicKey);
        });

        it('signs the input', function () {
            expect(
                verifySignature({
                    publicKey: senderWallet.publicKey,
                    data: transaction.outputMap,
                    signature: transaction.input.signature
                })
            ).toBe(true)
        });
    });

    describe('validTransaction()', () => {
        let errorMock;

        beforeEach(() => {
            errorMock = jest.fn();

            global.console.error = errorMock;
        })

        describe('when the transaction is valid', () => {
            it('returns true', function () {
                expect(Transaction.validTransaction(transaction)).toBe(true);
            });
        })

        describe('when the transaction is invalid', () => {
            describe('and a transaction outputMap value is invalid', () => {
                it('returns false and logs an error', function () {
                    transaction.outputMap[senderWallet.publicKey] = 999999;

                    expect(Transaction.validTransaction(transaction)).toBe(false);
                    expect(errorMock).toHaveBeenCalled();
                });
            })

            describe('and the transaction input signature is invalid', () => {
                it('returns false and logs an error', function () {
                    transaction.input.signature = new Wallet().sign('data');

                    expect(Transaction.validTransaction(transaction)).toBe(false);
                    expect(errorMock).toHaveBeenCalled();
                });
            })
        })
    });

    describe('update()', () => {
        let originalSignature, originalSenderOutput, nextRecipient, nextAmount;

        describe('and the amount is invalid', () => {
            it('throws an error', function () {
                expect(() => {
                    transaction.update({
                        senderWallet, recipient: 'foo', amount: 999999
                    })
                }).toThrow('Amount exceeds balance')
            });
        })

        describe('and the amount is valid', () => {
            beforeEach(() => {
                originalSignature = transaction.input.signature;
                originalSenderOutput = transaction.outputMap[senderWallet.publicKey];
                nextRecipient = 'next-recipient';
                nextAmount = 50;

                transaction.update({senderWallet, recipient: nextRecipient, amount: nextAmount})
            })

            it('outputs the amount to the next recipient', function () {
                expect(transaction.outputMap[nextRecipient]).toEqual(nextAmount);
            });

            it('subtracts the amount from the original sender output amount', function () {
                expect(transaction.outputMap[senderWallet.publicKey]).toEqual(originalSenderOutput - nextAmount);
            });

            it('maintains a total output that matches the input amount', function () {
                expect(Object.values(transaction.outputMap).reduce((total, outputMap) => total + outputMap))
                    .toEqual(transaction.input.amount);
            });

            it('re-signs the transaction', function () {
                expect(transaction.input.signature).not.toEqual(originalSignature);
            });

            describe('and another update for the same recipient', () => {
                let addedAmount;

                beforeEach(() => {
                    addedAmount = 80;
                    transaction.update(
                        {senderWallet, recipient: nextRecipient, amount: addedAmount}
                    )
                })

                it('adds to the recipient amount', function () {
                    expect(transaction.outputMap[nextRecipient]).toEqual(nextAmount + addedAmount);
                });

                it('subtracts the amount from the original sender output amount', function () {
                    expect(transaction.outputMap[senderWallet.publicKey]).toEqual(originalSenderOutput - nextAmount - addedAmount)
                });
            })
        })
    })
})